using DeLaSalle.Practica1.Core.Entities;
using DeLaSalle.Practica1.Core.Services.Interfaces;

namespace DeLaSalle.Practica1.Core.Services;

public class WeightMarsService : IWeightMarsService
{
    public WeightMars CalculateWeightMars(Person person)
    {
        //Weight on Mars= (Weight on Earth/9.81m/s2) * 3.711m/s2
        var wm = new WeightMars();
        wm.Weight = Math.Round(((person.Weight / 9.81f) * 3.711f), 2);
        
        return wm;
    }
}
