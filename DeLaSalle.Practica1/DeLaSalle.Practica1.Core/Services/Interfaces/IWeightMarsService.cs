using DeLaSalle.Practica1.Core.Entities;

namespace DeLaSalle.Practica1.Core.Services.Interfaces;

public interface IWeightMarsService
{
    WeightMars CalculateWeightMars(Person person);
}
