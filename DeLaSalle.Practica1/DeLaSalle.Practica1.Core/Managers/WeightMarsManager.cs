using DeLaSalle.Practica1.Core.Managers.Interfaces;
using DeLaSalle.Practica1.Core.Services.Interfaces;
using DeLaSalle.Practica1.Core.Entities;

namespace DeLaSalle.Practica1.Core.Managers;

public class WeightMarsManager : IWeightMarsManager
{
    private readonly IWeightMarsService _wmservice;

    public WeightMarsManager(IWeightMarsService wmservice)
    {
        _wmservice = wmservice;
    }

    public WeightMars GetWeightMars(Person person)
    {
        return _wmservice.CalculateWeightMars(person);
    }
}
