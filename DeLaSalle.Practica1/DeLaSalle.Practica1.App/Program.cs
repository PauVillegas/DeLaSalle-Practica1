﻿using DeLaSalle.Practica1.Core.Entities;
using DeLaSalle.Practica1.Core.Services;
using DeLaSalle.Practica1.Core.Managers;

namespace DeLaSalle.Practica1.App;

public class Program
{
    public static void Main(string[] args)
    {
        float weight = 0f;

        System.Console.WriteLine("Captura tu peso [KG]:");
        Single.TryParse(System.Console.ReadLine(), out weight);

        var person = new Person { Weight = weight };
        var service = new WeightMarsService();
        var manager = new WeightMarsManager(service);

        WeightMars ws = manager.GetWeightMars(person);

        System.Console.WriteLine($"Tu peso en Marte es {ws.Weight} KG");
    }
}
